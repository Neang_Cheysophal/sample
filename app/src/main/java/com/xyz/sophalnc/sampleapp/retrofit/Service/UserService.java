package com.xyz.sophalnc.sampleapp.retrofit.Service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserService {

    @GET("users")
    Call<ResponseBody> getUser();


}
