package com.xyz.sophalnc.sampleapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xyz.sophalnc.sampleapp.retrofit.Model.GithubModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;
import java.util.stream.Stream;

import static java.nio.file.Paths.get;

public class MainActivity extends AppCompatActivity {

    private Button btnCamera, btnGallery, btnServer,btnKhmer,btnEnglish;
    private ImageView ivImage;

    private int CAMERA_REQUEST = 1;
    private int GALLERY_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGUI();
        initEvent();
    }

    private void initGUI() {
        btnCamera = findViewById(R.id.btn_camera);
        btnGallery = findViewById(R.id.btn_gallery);
        ivImage = findViewById(R.id.iv_image);
        btnServer = findViewById(R.id.btn_server);
        btnEnglish = findViewById(R.id.btn_english);
        btnKhmer = findViewById(R.id.btn_khmer);
    }

    private void initEvent() {
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraIntent();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
            }
        });

        btnServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Configuration configuration = new Configuration();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    configuration.setLocale(locale);
                } else {
                    configuration.locale = locale;
                }
                onConfigurationChanged(configuration);
                Intent intent = getIntent();
                finishAffinity();
                startActivity(intent);
            }
        });

        btnKhmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Locale locale = new Locale("km");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Configuration configuration = new Configuration();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    configuration.setLocale(locale);
                } else {
                    configuration.locale = locale;
                }
                onConfigurationChanged(configuration);
                Intent intent = getIntent();
                finishAffinity();
                startActivity(intent);
            }
        });

    }

    private void getData() {
        DataManager.getInstance().getUserDataManager().getUser(onResponseListener);
    }

    private OnResponseListener onResponseListener = new OnResponseListener() {
        @Override
        public void onResponded(boolean isSuccess) {
            if (isSuccess) {
                GithubModel githubModel = DataManager.getInstance().getUserDataManager().getDataUser();
                Glide.with(getApplicationContext()).load(githubModel.getAvatarUrl()).into(ivImage);
                Log.d("Data", githubModel.toString());
            }
        }
    };

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, GALLERY_REQUEST);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            //Image Uri, u can convert to whatever u want to post to server
            storeBitmap(getSaveProfilePictureDirectory(this) + "profilePicture.png", (Bitmap) data.getExtras().get("data"));
            Uri imageUri = Uri.fromFile(new File(getSaveProfilePictureDirectory(this) + "profilePicture.png"));
            Glide.with(this).load(imageUri).into(ivImage);
        } else if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            final Uri imageUri = data.getData(); //Image Uri, u can convert to whatever u want to post to server
            Glide.with(this).load(imageUri).into(ivImage);
        }
    }

    private void storeBitmap(String path, Bitmap bitmap) {
        try {
            File file = new File(path);
            OutputStream out = null;
            try {
                out = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getSaveProfilePictureDirectory(Context context) {
        String path = context.getCacheDir().toString() + "/" + "Images" + "/";
        File f = new File(path);
        if (!f.exists()) {
            if (!f.mkdirs()) {
                return null;
            }
        }
        return path;
    }
}
