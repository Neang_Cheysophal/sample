package com.xyz.sophalnc.sampleapp;

import android.content.Context;

import com.xyz.sophalnc.sampleapp.retrofit.ApiHelper;

public class DataManager {
    private static DataManager mInstance;
    private Context mContext;
    private UserDataManager userDataManager;

    private DataManager() {
        userDataManager = new UserDataManager();
    }

    public static synchronized DataManager getInstance() {
        if (null == mInstance) {
            mInstance = new DataManager();
        }
        return mInstance;
    }

    public UserDataManager getUserDataManager() {
        return userDataManager;
    }

}
