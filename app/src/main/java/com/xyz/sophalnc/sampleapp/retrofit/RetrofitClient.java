package com.xyz.sophalnc.sampleapp.retrofit;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Socheat on 2/19/2017.
 */

public class RetrofitClient {

    private static Retrofit mInstance;
    private static Retrofit mGoogleInstance;
    private static Retrofit mInstagramInstance;

    public static synchronized Retrofit getClient(String baseUrl) {

        if (null == mInstance) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            if (ApiHelper.SHOW_LOG)
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(1, TimeUnit.MINUTES);
            httpClient.connectTimeout(1, TimeUnit.MINUTES);
            httpClient.addInterceptor(logging);
            mInstance = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mInstance;
    }
}
