package com.xyz.sophalnc.sampleapp;

import android.content.Context;

import com.xyz.sophalnc.sampleapp.retrofit.ApiHelper;
import com.xyz.sophalnc.sampleapp.retrofit.Model.GithubModel;
import com.xyz.sophalnc.sampleapp.retrofit.Service.UserService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataManager {

    private String mErrorMessage = "";

    GithubModel githubData;

    public void getUser(final OnResponseListener onResponseListener) {
        UserService userService = ApiHelper.getUserService();
        userService.getUser().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String str = response.body().string();
                            JSONArray jsonObject = new JSONArray(str);


                                githubData = parseGithubModel(jsonObject.getJSONObject(0));
                                if (null != onResponseListener) {
                                    onResponseListener.onResponded(true);
                                }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (null != onResponseListener) {
                    onResponseListener.onResponded(false);
                }
            }
        });
    }

    GithubModel parseGithubModel(JSONObject jsonObject) {
        GithubModel githubModel = new GithubModel();
        githubModel.setLogin(jsonObject.optString("login"));
        githubModel.setId(jsonObject.optInt("id"));
        githubModel.setNodeId(jsonObject.optString("node_id"));
        githubModel.setAvatarUrl(jsonObject.optString("avatar_url"));
        githubModel.setGravatarId(jsonObject.optString("gravatar_id"));
        githubModel.setGravatarId(jsonObject.optString("url"));
        githubModel.setHtmlUrl(jsonObject.optString("html_url"));
        githubModel.setFollowersUrl(jsonObject.optString("followers_url"));
        githubModel.setFollowingUrl(jsonObject.optString("following_url"));
        githubModel.setGistsUrl(jsonObject.optString("gists_url"));
        githubModel.setStarredUrl(jsonObject.optString("starred_url"));
        githubModel.setSubscriptionsUrl(jsonObject.optString("subscriptions_url"));
        githubModel.setOrganizationsUrl(jsonObject.optString("organizations_url"));
        githubModel.setReposUrl(jsonObject.optString("repos_url"));
        githubModel.setEventsUrl(jsonObject.optString("events_url"));
        githubModel.setReceivedEventsUrl(jsonObject.optString("received_events_url"));
        githubModel.setType(jsonObject.optString("type"));
        githubModel.setSiteAdmin(jsonObject.optBoolean("site_admin"));
        return githubModel;
    }

    public GithubModel getDataUser() {
        return githubData;
    }
}
