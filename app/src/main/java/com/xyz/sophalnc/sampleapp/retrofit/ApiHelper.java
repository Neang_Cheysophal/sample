package com.xyz.sophalnc.sampleapp.retrofit;

import com.xyz.sophalnc.sampleapp.retrofit.Service.UserService;

/**
 * Created by Socheat on 2/19/2017.
 */

public class ApiHelper {
    public static boolean SHOW_LOG = true;

    public static final String BASE_URL = "https://api.github.com/";

    public static UserService getUserService() {
        return RetrofitClient.getClient(BASE_URL).create(UserService.class);
    }

}
